const data = [
  {
    parameters: {
      Status: 'Down',
      SSID: 'prplOS',
      LowerLayers: 'WiFi.Radio.wifi0',
      BSSID: 'b2:83:c4:06:de:79',
      MACAddress: 'B2:83:C4:06:DE:79',
      Enable: 1,
      Index: 7,
      Name: 'vap2g0priv',
      Alias: 'wlan0',
    },
    path: 'WiFi.SSID.1.',
  },
  {
    parameters: {
      MulticastPacketsSent: 0,
      ErrorsSent: 0,
      BroadcastPacketsSent: 0,
      BytesSent: 0,
      PacketsSent: 0,
      BytesReceived: 0,
      DiscardPacketsReceived: 0,
      ErrorsReceived: 0,
      MulticastPacketsReceived: 0,
      UnknownProtoPacketsReceived: 0,
      MultipleRetryCount: 0,
      FailedRetransCount: 0,
      UnicastPacketsSent: 0,
      UnicastPacketsReceived: 0,
      PacketsReceived: 0,
      DiscardPacketsSent: 0,
      RetransCount: 0,
      BroadcastPacketsReceived: 0,
      RetryCount: 0,
    },
    path: 'WiFi.SSID.1.Stats.',
  },
  {
    parameters: { AC_VI: 0, AC_BK: 0, AC_BE: 0, AC_VO: 0 },
    path: 'WiFi.SSID.1.Stats.WmmBytesReceived.',
  },
  {
    parameters: { AC_VI: 0, AC_BK: 0, AC_BE: 0, AC_VO: 0 },
    path: 'WiFi.SSID.1.Stats.WmmBytesSent.',
  },
  {
    parameters: { AC_VI: 0, AC_BK: 0, AC_BE: 0, AC_VO: 0 },
    path: 'WiFi.SSID.1.Stats.WmmFailedBytesReceived.',
  },
  {
    parameters: { AC_VI: 0, AC_BK: 0, AC_BE: 0, AC_VO: 0 },
    path: 'WiFi.SSID.1.Stats.WmmFailedReceived.',
  },
  {
    parameters: { AC_VI: 0, AC_BK: 0, AC_BE: 0, AC_VO: 0 },
    path: 'WiFi.SSID.1.Stats.WmmFailedSent.',
  },
  {
    parameters: { AC_VI: 0, AC_BK: 0, AC_BE: 0, AC_VO: 0 },
    path: 'WiFi.SSID.1.Stats.WmmFailedbytesSent.',
  },
  {
    parameters: { AC_VI: 0, AC_BK: 0, AC_BE: 0, AC_VO: 0 },
    path: 'WiFi.SSID.1.Stats.WmmPacketsReceived.',
  },
  {
    parameters: { AC_VI: 0, AC_BK: 0, AC_BE: 0, AC_VO: 0 },
    path: 'WiFi.SSID.1.Stats.WmmPacketsSent.',
  },
];

export { data };
