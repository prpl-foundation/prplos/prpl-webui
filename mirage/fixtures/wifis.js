export default [
  { parameters: {}, path: 'WiFi.' },
  {
    parameters: { Enable: 1, DelayTime: 1000, BootDelayTime: 4000 },
    path: 'WiFi.AutoCommitMgr.',
  },
];
