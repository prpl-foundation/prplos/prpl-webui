const data = [
  {
    parameters: {
      SupportedStandards: 'a,n,an,ac',
      ChannelsInUse: '36,40,44,48,',
      Channel: 36,
      TransmitPowerSupported: '6,12,25,50,100,-1',
      Name: 'wlan1',
      dbgRADFile: '',
      MCS: 9,
      IEEE80211rSupported: 1,
      IntelligentAirtimeSchedulingEnable: 1,
      dbgRADEnable: 0,
      Upstream: 0,
      RetryLimit: 7,
      RxPowerSaveRepeaterEnable: 0,
      STA_Mode: 0,
      OperatingClass: 128,
      IEEE80211kSupported: 1,
      TransmitPower: -1,
      AutoChannelRefreshPeriod: 0,
      DFSChannelChangeEventTimestamp: '1970-01-01T00:00:00Z',
      WDS_Mode: 1,
      Interference: 0,
      Noise: 0,
      ImplicitBeamFormingEnabled: 1,
      BaseMACAddress: 'B2:83:C4:06:DE:7A',
      HeCapsEnabled: '',
      ActiveAssociatedDevices: 0,
      GuardInterval: 'Auto',
      STASupported_Mode: 0,
      MultiUserMIMOSupported: 0,
      RIFSEnabled: 'Default',
      HeCapsSupported: '',
      PossibleChannels:
        '36,40,44,48,52,56,60,64,100,104,108,112,116,120,124,128,132,136,140,144,149,153,157,161,165',
      IEEE80211_Caps: 'UAPSD WEP TKIP AES AES_CCM SAE EXPL_BF IMPL_BF CSA',
      AirtimeFairnessEnabled: 1,
      AutoChannelEnable: 0,
      ChannelBandwidthChangeReason: 'AUTO',
      OperatingChannelBandwidth: 'Auto',
      IEEE80211hEnabled: 1,
      ExtensionChannel: 'Auto',
      ExplicitBeamFormingSupported: 1,
      RxBeamformingCapsAvailable: 'VHT_SU_BF,VHT_MU_BF',
      AutoBandwidthSelectMode: 'MaxAvailable',
      DTIMPeriod: 3,
      ActiveAntennaCtrl: 0,
      ImplicitBeamFormingSupported: 1,
      AutoChannelSupported: 1,
      Index: 8,
      MaxChannelBandwidth: '80MHz',
      RxChainCtrl: -1,
      MaxAssociatedDevices: 64,
      WPS_Enrollee_Mode: 0,
      MultiAPTypesSupported: 'FronthaulBSS,BackhaulBSS,BackhaulSTA',
      DelayApUpPeriod: 10,
      ChannelLoad: 0,
      RxPowerSaveEnabled: 1,
      TxBeamformingCapsEnabled: 'DEFAULT',
      OperatingStandardsFormat: 'Legacy',
      TxChainCtrl: -1,
      ExplicitBeamFormingEnabled: 1,
      ObssCoexistenceEnable: 0,
      LongRetryLimit: 0,
      ProbeRequestNotify: 'FirstRSSI',
      RegulatoryDomain: 'DE',
      IEEE80211hSupported: 1,
      TxBeamformingCapsAvailable: 'VHT_SU_BF,VHT_MU_BF',
      ProbeRequestAggregationTimer: 0,
      WET_Mode: 0,
      Status: 'Dormant',
      Enable: 1,
      CurrentOperatingChannelBandwidth: '80MHz',
      OfdmaEnable: 1,
      KickRoamingStation: 1,
      AP_Mode: 1,
      DFSChannelChangeEventCounter: 0,
      MaxBitRate: 0,
      OperatingStandards: 'auto',
      VendorPCISig: 'nl80211',
      RxBeamformingCapsEnabled: 'DEFAULT',
      MultiUserMIMOEnabled: 1,
      ChannelChangeReason: 'INITIAL',
      OperatingFrequencyBand: '5GHz',
      BeaconPeriod: 100,
      Alias: '',
      TargetWakeTimeEnable: 1,
    },
    path: 'WiFi.Radio.2.',
  },
  {
    parameters: { FileLogLimit: 4, EventLogLimit: 4 },
    path: 'WiFi.Radio.2.DFS.',
  },
  {
    parameters: {
      FragmentationThreshold: -1,
      RtsThreshold: -1,
      BroadcastMaxBwCapability: -1,
      Ampdu: -1,
      TxBurst: -1,
      Amsdu: -1,
      TPCMode: 'Auto',
      VhtOmnEnabled: 1,
      TxBeamforming: -1,
    },
    path: 'WiFi.Radio.2.DriverConfig.',
  },
  {
    parameters: {
      NrRxAntenna: 2,
      NrActiveTxAntenna: 2,
      NrActiveRxAntenna: 2,
      NrTxAntenna: 2,
    },
    path: 'WiFi.Radio.2.DriverStatus.',
  },
  {
    parameters: {
      LastOccurrence: '',
      Info: '0x0001610f 0x00000000 / 0x00000000 0x00000000',
      Key: '',
      Value: 1,
    },
    path: 'WiFi.Radio.2.EventCounter.1.',
  },
  {
    parameters: {
      LastOccurrence: '0001-01-01T00:00:00Z',
      Info: '',
      Key: '',
      Value: 0,
    },
    path: 'WiFi.Radio.2.EventCounter.2.',
  },
  {
    parameters: {
      LastOccurrence: '0001-01-01T00:00:00Z',
      Info: '',
      Key: '',
      Value: 0,
    },
    path: 'WiFi.Radio.2.EventCounter.3.',
  },
  {
    parameters: {
      LastOccurrence: '0001-01-01T00:00:00Z',
      Info: '',
      Key: '',
      Value: 0,
    },
    path: 'WiFi.Radio.2.EventCounter.4.',
  },
  {
    parameters: {
      UseBaseMacOffset: 1,
      UseLocalBitForGuest: 0,
      NrBssRequired: 0,
      LocalGuestMacOffset: 256,
      BaseMacOffset: 2,
    },
    path: 'WiFi.Radio.2.MACConfig.',
  },
  { parameters: { Enable: 1 }, path: 'WiFi.Radio.2.NaStaMonitor.' },
  {
    parameters: {
      RssiInterval: 10,
      Enable: 0,
      AveragingFactor: 500,
      Interval: 1000,
    },
    path: 'WiFi.Radio.2.NaStaMonitor.RssiEventing.',
  },
  {
    parameters: {
      Available: '',
      Enabled: 'DFS_AHEAD DELAY_COMMIT',
      Status: '',
    },
    path: 'WiFi.Radio.2.RadCaps.',
  },
  {
    parameters: {
      FastScanReasons: 'Ssid',
      MaxChannelsPerScan: -1,
      ScanRequestInterval: -1,
      HomeTime: -1,
      PassiveChannelTime: -1,
      ScanChannelCount: -1,
      BlockScanMode: 'Disable',
      ActiveChannelTime: -1,
    },
    path: 'WiFi.Radio.2.ScanConfig.',
  },
  { parameters: { NrCoChannelAP: 0 }, path: 'WiFi.Radio.2.ScanResults.' },
  {
    parameters: {
      NrScanRequested: 0,
      NrScanDone: 0,
      NrScanBlocked: 0,
      NrScanError: 0,
    },
    path: 'WiFi.Radio.2.ScanStats.',
  },
  {
    parameters: {
      MulticastPacketsSent: 0,
      ErrorsSent: 0,
      BroadcastPacketsSent: 0,
      BytesSent: 0,
      PacketsSent: 0,
      BytesReceived: 0,
      DiscardPacketsReceived: 0,
      ErrorsReceived: 0,
      MulticastPacketsReceived: 0,
      MultipleRetryCount: 0,
      Temperature: -274,
      FailedRetransCount: 0,
      UnicastPacketsSent: 0,
      UnicastPacketsReceived: 0,
      PacketsReceived: 0,
      DiscardPacketsSent: 0,
      RetransCount: 0,
      BroadcastPacketsReceived: 0,
      RetryCount: 0,
    },
    path: 'WiFi.Radio.2.Stats.',
  },
  {
    parameters: { AC_VI: 0, AC_BK: 0, AC_BE: 0, AC_VO: 0 },
    path: 'WiFi.Radio.2.Stats.WmmBytesReceived.',
  },
  {
    parameters: { AC_VI: 0, AC_BK: 0, AC_BE: 0, AC_VO: 0 },
    path: 'WiFi.Radio.2.Stats.WmmBytesSent.',
  },
  {
    parameters: { AC_VI: 0, AC_BK: 0, AC_BE: 0, AC_VO: 0 },
    path: 'WiFi.Radio.2.Stats.WmmFailedBytesReceived.',
  },
  {
    parameters: { AC_VI: 0, AC_BK: 0, AC_BE: 0, AC_VO: 0 },
    path: 'WiFi.Radio.2.Stats.WmmFailedReceived.',
  },
  {
    parameters: { AC_VI: 0, AC_BK: 0, AC_BE: 0, AC_VO: 0 },
    path: 'WiFi.Radio.2.Stats.WmmFailedSent.',
  },
  {
    parameters: { AC_VI: 0, AC_BK: 0, AC_BE: 0, AC_VO: 0 },
    path: 'WiFi.Radio.2.Stats.WmmFailedbytesSent.',
  },
  {
    parameters: { AC_VI: 0, AC_BK: 0, AC_BE: 0, AC_VO: 0 },
    path: 'WiFi.Radio.2.Stats.WmmPacketsReceived.',
  },
  {
    parameters: { AC_VI: 0, AC_BK: 0, AC_BE: 0, AC_VO: 0 },
    path: 'WiFi.Radio.2.Stats.WmmPacketsSent.',
  },
  { parameters: {}, path: 'WiFi.Radio.2.Vendor.' },
];

export { data };
