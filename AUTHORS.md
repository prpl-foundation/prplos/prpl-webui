<!--
SPDX-License-Identifier: BSD-2-Clause-Patent
Copyright (c) 2022 the prpl contributors
This code is subject to the terms of the BSD+Patent license.
See LICENSE file for more details.
-->
The prpl-webuid project is copyright 2022 its contributors.
This file lists all contributors.

Individuals:
- Peter Steinhäuser (embeDD.com)
