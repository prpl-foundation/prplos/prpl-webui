import Model, { attr, hasMany } from '@ember-data/model';

export default class WiFiModel extends Model {
  @hasMany('wifi-accesspoint') AccessPoint;
  @hasMany('wifi-radio') Radio;
  @hasMany('wifi-ssid') SSID;

  @attr({
    defaultValue() { return 'WiFi.'; }
  }) _namespace;
}
