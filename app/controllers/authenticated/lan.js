import Controller from '@ember/controller';

export default class AuthenticatedLanController extends Controller {
  get interfaces() {
    return this.model.Interface.filter((iface) => {
      return iface.Alias !== 'wan' && iface.Alias !== 'loopback';
    });
  }
}
