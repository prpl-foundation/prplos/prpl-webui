import ApplicationAdapter from './application';

export default class SoftwaremodulesAdapter extends ApplicationAdapter {
  pathForType(type) {
    return 'SoftwareModules.';
  }
}
