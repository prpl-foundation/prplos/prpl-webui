import ApplicationAdapter from './application';

export default class DeviceinfoAdapter extends ApplicationAdapter {
  pathForType(type) {
    return 'DeviceInfo.';
  }
}
